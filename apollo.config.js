module.exports = {
    client: {
        name: 'meet-it-client',
        service: {
            name: 'meet-it-server',
            url: 'http://localhost:4000/graphql',
            skipSSLValidation: true
        },
        includes: [
            __dirname+'/src/app/shared/models/queries/**',
            __dirname+'/src/app/shared/models/mutations/**'
        ]
    }
};
