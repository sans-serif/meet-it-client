import {Injectable} from '@angular/core';
import gql from 'graphql-tag';
import {Mutation} from 'apollo-angular';

@Injectable({
    providedIn: 'root'
})
export class SignInService extends Mutation {
    document = gql`
        mutation signIn($email: String!, $password: String!){
            signIn(email: $email, password: $password) {
                userId
                token
                tokenExpiration
            }
        }`;
}
