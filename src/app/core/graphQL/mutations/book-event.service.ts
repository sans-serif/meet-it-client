import {Injectable} from '@angular/core';
import {Mutation} from 'apollo-angular';
import gql from 'graphql-tag';

@Injectable({
    providedIn: 'root'
})
export class BookEventService extends Mutation {

    document = gql`
        mutation bookEvent($eventId: ID!) {
            bookEvent(eventId: $eventId) {
                _id
                event {
                    _id
                    title
                    creator {
                        email
                    }
                }
                user {
                    email
                }
            }

        }`;
}
