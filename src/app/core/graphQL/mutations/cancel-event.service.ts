import {Injectable} from '@angular/core';
import {Mutation} from 'apollo-angular';
import gql from 'graphql-tag';

@Injectable({
    providedIn: 'root'
})
export class CancelEventService extends Mutation {

    document = gql`
        mutation cancelEvent($eventId: ID!){
            cancelEvent(eventId: $eventId) {
                _id
                title
            }
        }`;
}
