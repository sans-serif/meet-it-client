import {Injectable} from '@angular/core';
import {Mutation} from 'apollo-angular';
import gql from 'graphql-tag';

@Injectable({
    providedIn: 'root'
})
export class CancelBookingService extends Mutation {

    document = gql`
        mutation cancelBooking($bookingId: ID!) {
            cancelBooking(bookingId: $bookingId) {
                _id
                event {
                    _id
                }
            }
        }`;
}
