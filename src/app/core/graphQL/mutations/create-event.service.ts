import {Injectable} from '@angular/core';
import gql from 'graphql-tag';
import {Mutation} from 'apollo-angular';

@Injectable({
    providedIn: 'root'
})
export class CreateEventService extends Mutation {

    document = gql`
        mutation createEvent($title: String!, $description: String!, $price: Float!, $date: String!) {
            createEvent(eventInput:{title: $title, description: $description, price: $price, date: $date}) {
                _id
                title
                description
                price
                date
                creator {
                    email
                }
            }
        }`;
}
