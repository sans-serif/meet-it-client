import {Injectable} from '@angular/core';
import gql from 'graphql-tag';
import {Mutation} from 'apollo-angular';

@Injectable({
    providedIn: 'root'
})
export class SignUpService extends Mutation {
    document = gql`
        mutation singUp($email: String!, $password: String!){
            signUp(userInput: { email: $email, password: $password }) {
                _id
                email
            }
        }`;
}
