import {Injectable} from '@angular/core';
import {Query} from 'apollo-angular';
import gql from 'graphql-tag';

@Injectable({
    providedIn: 'root'
})
export class BookingService extends Query<Response> {
    document = gql`
        query boking ($eventId: ID!){
            booking(eventId: $eventId) {
                _id
                user{
                    email
                }
                event{
                    title
                }

            }
        }`;
}
