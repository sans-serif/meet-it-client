import {Injectable} from '@angular/core';
import {Query} from 'apollo-angular';
import gql from 'graphql-tag';

@Injectable({
    providedIn: 'root'
})
export class ActiveEventsService extends Query<Response> {

    document = gql`
        query activeEvents{
            activeEvents{
                _id
                title
                canceled
                price
                date
                belongsToCurrentUser
                wasBookedByCurrentUser
                creator{
                    email
                }
            }
        }
    `;
}
