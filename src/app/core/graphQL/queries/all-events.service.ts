import {Injectable} from '@angular/core';
import {Query} from 'apollo-angular';
import gql from 'graphql-tag';

@Injectable({
    providedIn: 'root'
})
export class AllEventsService extends Query<Response> {
    document = gql`
        query events {
            events {
                _id
                title
                canceled
                price
                date
                belongsToCurrentUser
                wasBookedByCurrentUser
                creator{
                    email
                }
            }
        }`;
}
