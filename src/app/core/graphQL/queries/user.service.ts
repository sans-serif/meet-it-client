import {Injectable} from '@angular/core';
import {Query} from 'apollo-angular';
import gql from 'graphql-tag';

@Injectable({
    providedIn: 'root'
})
export class UserService extends Query<Response> {
    document = gql`
        query user {
            user {
                _id
                email
            }
        }`;
}
