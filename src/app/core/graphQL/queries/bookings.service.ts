import {Injectable} from '@angular/core';
import {Query} from 'apollo-angular';
import gql from 'graphql-tag';

@Injectable({
    providedIn: 'root'
})
export class BookingsService extends Query<Response> {
    document = gql`
        query bookings {
            bookings {
                _id
                event {
                    title
                }
            }
        }`;
}
