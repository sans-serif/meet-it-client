import {Injectable} from '@angular/core';
import {Query} from 'apollo-angular';
import gql from 'graphql-tag';

@Injectable({
    providedIn: 'root'
})
export class UserEventsService extends Query<Response> {
    document = gql`
        query userEvents {
            userEvents {
                _id
                title
                canceled
                price
                date
                creator{
                    email
                }
            }
        }`;
}
