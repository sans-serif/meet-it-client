import {Injectable} from '@angular/core';
import {Query} from 'apollo-angular';
import gql from 'graphql-tag';

@Injectable({
    providedIn: 'root'
})
export class EventService extends Query<Response> {
    document = gql`
        query event($eventId: ID!) {
            event(eventId: $eventId) {
                _id
                title
                description
                canceled
                price
                date
                belongsToCurrentUser
                wasBookedByCurrentUser
                creator {
                    email
                }
            }
        }`;
}
