import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpHeaders, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {AuthHelperService} from '../services/auth/auth-helper.service';
import {signIn_signIn} from '../../shared/models/types/signIn';

@Injectable({
    providedIn: 'root'
})
export class TokenService implements HttpInterceptor {
    constructor(private authHelper: AuthHelperService) {
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (this.authHelper.isAuthenticated()) {
            const authData: signIn_signIn = AuthHelperService.getAuthData();
            const headers = new HttpHeaders().set('Authorization', `Bearer ${authData.token}`);
            const authRequest = req.clone({headers});

            return next.handle(authRequest);
        } else {
            return next.handle(req);
        }
    }
}
