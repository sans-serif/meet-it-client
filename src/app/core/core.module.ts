import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GraphQLModule } from '../graphql.module';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { TokenService } from './interceptors/token.service';
import { AuthHelperService } from './services/auth/auth-helper.service';
import { EventsHelperService } from './services/events/events-helper.service';
import { AuthRepositoryService } from './repositories/auth/auth-repository.service';
import { EventsRepositoryService } from './repositories/events/events-repository.service';
import { JWT_OPTIONS, JwtHelperService } from '@auth0/angular-jwt';

@NgModule({
    declarations: [],
    imports: [
        CommonModule,
        GraphQLModule,
        HttpClientModule
    ],
    providers: [
        AuthHelperService,
        EventsHelperService,
        AuthRepositoryService,
        EventsRepositoryService,
        JwtHelperService,
        { provide: JWT_OPTIONS, useValue: JWT_OPTIONS },
        { provide: HTTP_INTERCEPTORS, useClass: TokenService, multi: true }
    ]
})
export class CoreModule {
}
