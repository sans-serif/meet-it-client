import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {ActiveEventsService} from '../../graphQL/queries/active-events.service';
import {UserEventsService} from '../../graphQL/queries/user-events.service';
import {AllEventsService} from '../../graphQL/queries/all-events.service';
import {EventService} from '../../graphQL/queries/event.service';
import {CreateEventService} from '../../graphQL/mutations/create-event.service';
import {CancelEventService} from '../../graphQL/mutations/cancel-event.service';
import {UpdateEventService} from '../../graphQL/mutations/update-event.service';

@Injectable({
    providedIn: 'root'
})
export class EventsRepositoryService {

    constructor(
        private allEventsGQL: AllEventsService,
        private activeEventsGQL: ActiveEventsService,
        private userEventsGQL: UserEventsService,
        private eventGQL: EventService,
        private createEventGQL: CreateEventService,
        private updateEventGQL: UpdateEventService,
        private cancelEventGQL: CancelEventService) {
    }

    private static handleError(err) {
        if (err.graphQLErrors.length > 0) {
            throw err.graphQLErrors;
        }

        if (err.networkError) {
            throw err.networkError;
        }

        return of([]);
    }

    loadAllEvents(): Observable<any> {
        return this.allEventsGQL.watch()
            .valueChanges
            .pipe(
                map((result) => result.data),
                catchError((err: any) => EventsRepositoryService.handleError(err))
            );
    }

    loadActiveEvents(): Observable<any> {
        return this.activeEventsGQL.watch()
            .valueChanges
            .pipe(
                map((result) => result.data),
                catchError((err: any) => EventsRepositoryService.handleError(err))
            );
    }

    loadUserEvents(): Observable<any> {
        return this.userEventsGQL.watch()
            .valueChanges
            .pipe(
                map((result) => result.data),
                catchError((err: any) => EventsRepositoryService.handleError(err))
            );
    }

    loadEvent(eventId): Observable<any> {
        return this.eventGQL.watch({eventId})
            .valueChanges
            .pipe(
                map((result) => result.data),
                catchError((err: any) => EventsRepositoryService.handleError(err))
            );
    }

    createEvent(newEvent): Observable<any> {
        return this.createEventGQL.mutate({
            title: newEvent.title,
            description: newEvent.description,
            price: newEvent.price,
            date: newEvent.date
        }).pipe(
            map((result) => result.data),
            catchError((err: any) => EventsRepositoryService.handleError(err))
        );
    }

    updateEvent(event): Observable<any> {
        return this.updateEventGQL.mutate({
            title: event.title,
            description: event.description,
            price: event.price,
            date: event.date
        }).pipe(
            map((result) => result.data),
            catchError((err: any) => EventsRepositoryService.handleError(err))
        );
    }

    cancelEvent(eventId): Observable<any> {
        return this.cancelEventGQL.mutate({eventId})
            .pipe(
                map((result) => result.data),
                catchError((err: any) => EventsRepositoryService.handleError(err))
            );
    }
}
