import { TestBed } from '@angular/core/testing';

import { EventsRepositoryService } from './events-repository.service';

describe('EventsRepositoryService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EventsRepositoryService = TestBed.get(EventsRepositoryService);
    expect(service).toBeTruthy();
  });
});
