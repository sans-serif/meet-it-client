import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {BookingsService} from '../../graphQL/queries/bookings.service';
import {BookingService} from '../../graphQL/queries/booking.service';
import {BookEventService} from '../../graphQL/mutations/book-event.service';
import {CancelBookingService} from '../../graphQL/mutations/cancel-booking.service';

@Injectable({
    providedIn: 'root'
})
export class BookingsRepositoryService {

    constructor(
        private bookingsGQL: BookingsService,
        private bookingGQL: BookingService,
        private bookEventGQL: BookEventService,
        private cancelBookingGQL: CancelBookingService
    ) {
    }

    private static handleError(err) {
        if (err.graphQLErrors.length > 0) {
            throw err.graphQLErrors;
        }

        if (err.networkError) {
            throw err.networkError;
        }

        return of([]);
    }

    loadBookings(): Observable<any> {
        return this.bookingsGQL.watch()
            .valueChanges
            .pipe(
                map((result) => result.data),
                catchError((err: any) => BookingsRepositoryService.handleError(err))
            );
    }

    loadBooking(eventId): Observable<any> {
        return this.bookingGQL.watch({eventId})
            .valueChanges
            .pipe(
                map((result) => result.data),
                catchError((err: any) => BookingsRepositoryService.handleError(err))
            );
    }

    bookEvent(eventId): Observable<any> {
        return this.bookEventGQL.mutate({eventId})
            .pipe(
                map((result) => result.data),
                catchError((err: any) => BookingsRepositoryService.handleError(err))
            );
    }

    cancelBooking(bookingId): Observable<any> {
        return this.cancelBookingGQL.mutate({bookingId})
            .pipe(
                map((result) => result.data),
                catchError((err: any) => BookingsRepositoryService.handleError(err))
            );
    }
}
