import { TestBed } from '@angular/core/testing';

import { BookingsRepositoryService } from './bookings-repository.service';

describe('BookingsRepositoryService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BookingsRepositoryService = TestBed.get(BookingsRepositoryService);
    expect(service).toBeTruthy();
  });
});
