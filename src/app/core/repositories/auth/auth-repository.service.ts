import {Injectable} from '@angular/core';
import {Apollo} from 'apollo-angular';
import {Observable, of} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {signIn, signInVariables} from 'src/app/shared/models/types/signIn';
import {signUp, signUpVariables} from '../../../shared/models/types/signUp';
import {UserService} from '../../graphQL/queries/user.service';
import {SignInService} from '../../graphQL/mutations/sign-in.service';
import {SignUpService} from '../../graphQL/mutations/sign-up.service';

@Injectable({
    providedIn: 'root'
})
export class AuthRepositoryService {

    constructor(
        private apollo: Apollo,
        private userGQL: UserService,
        private singUpGQL: SignUpService,
        private signInGQL: SignInService) {
    }

    private static handleError(err) {
        if (err.graphQLErrors.length > 0) {
            throw err.graphQLErrors;
        }

        if (err.networkError) {
            throw err.networkError;
        }

        return of([]);
    }

    signUp(user: signUpVariables): Observable<signUp> {
        return this.singUpGQL.mutate({
            email: user.email,
            password: user.password
        })
            .pipe(
                map(({data}: any) => data),
                catchError((err: any) => AuthRepositoryService.handleError(err))
            );
    }

    signIn(user: signInVariables): Observable<signIn> {
        return this.signInGQL.mutate({
            email: user.email,
            password: user.password
        })
            .pipe(
                map(({data}: any) => data),
                catchError((err: any) => AuthRepositoryService.handleError(err))
            );
    }

    singOut() {
        this.apollo.getClient().resetStore();
    }

    getUser(): Observable<any> {
        return this.userGQL.watch()
            .valueChanges
            .pipe(
                map((result: any) => result.data),
                catchError((err: any) => AuthRepositoryService.handleError(err))
            );
    }
}
