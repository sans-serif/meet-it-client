import { TestBed } from '@angular/core/testing';

import { EventsHelperService } from './events-helper.service';

describe('EventsHelperService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EventsHelperService = TestBed.get(EventsHelperService);
    expect(service).toBeTruthy();
  });
});
