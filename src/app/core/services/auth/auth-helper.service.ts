import {Injectable} from '@angular/core';
import {JwtHelperService} from '@auth0/angular-jwt';
import {UserData} from '../../../shared/models/user-data';
import {signIn_signIn} from '../../../shared/models/types/signIn';


@Injectable({
    providedIn: 'root'
})
export class AuthHelperService {

    constructor(public jwtHelper: JwtHelperService) {
    }

    public redirectUrl: string;

    public static setAuthData(authData: signIn_signIn) {
        localStorage.setItem('authData', JSON.stringify(authData));
    }

    public static getAuthData(): signIn_signIn {
        return JSON.parse(localStorage.getItem('authData'));
    }

    public static setUserData(userData: UserData) {
        localStorage.setItem('userData', JSON.stringify(userData));
    }

    public static getUserData(): UserData {
        return JSON.parse(localStorage.getItem('userData'));
    }

    public static logOut() {
        localStorage.clear();
    }

    public getUserDataFromToken(): any {
        const token: string = JSON.parse(localStorage.getItem('authData')).token;
        return this.jwtHelper.decodeToken(token);
    }

    public isAuthenticated(): boolean {
        const authData = JSON.parse(localStorage.getItem('authData')) || '';
        return !this.jwtHelper.isTokenExpired(authData.token);
    }
}
