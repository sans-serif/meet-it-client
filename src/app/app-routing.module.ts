import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {AuthComponent} from './modules/auth/auth.component';
import {PublicComponent} from './modules/public/public.component';
import {AdminComponent} from './modules/admin/admin.component';
import {AuthGuard} from './core/guards/auth.guard';
import {PageNotFoundComponent} from './shared/page-not-found/page-not-found.component';

const routes: Routes = [
    {
        path: 'auth',
        component: AuthComponent,
        loadChildren: './modules/auth/auth.module#AuthModule',
    },
    {
        path: 'admin',
        component: AdminComponent,
        loadChildren: './modules/admin/admin.module#AdminModule',
        canActivate: [AuthGuard]
    },
    {
        path: 'public',
        component: PublicComponent,
        loadChildren: './modules/public/public.module#PublicModule'
    },
    {
        path: '**',
        component: PageNotFoundComponent
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
