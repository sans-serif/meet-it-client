export interface UserData {
    userId: string;
    email: string;
    iat: number;
    exp: number;
}
