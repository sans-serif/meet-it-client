import gql from 'graphql-tag';

export const signUp = gql`
    mutation signUp($email: String!, $password: String!){
        signUp(userInput: { email: $email, password: $password }) {
            _id
            email
        }
    }
`;

export const signIn = gql`
    mutation signIn($email: String!, $password: String!){
        signIn(email: $email, password: $password) {
            userId
            token
            tokenExpiration
        }
    }
`;
