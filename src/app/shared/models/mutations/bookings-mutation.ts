import gql from 'graphql-tag';

export const book = gql`
    mutation bookEvent($eventId: ID!) {
        bookEvent(eventId: $eventId) {
            _id
            event {
                _id
                title
                creator {
                    email
                }
            }
            user {
                email
            }
        }

    }
`;

export const cancel = gql`
    mutation cancelBooking($bookingId: ID!) {
        cancelBooking(bookingId: $bookingId) {
            _id
            event {
                _id
            }
        }
    }
`;
