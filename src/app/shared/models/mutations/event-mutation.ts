import gql from 'graphql-tag';

export const create = gql`
    mutation createEvent($title: String!, $description: String!, $price: Float!, $date: String!) {
        createEvent(eventInput:{title: $title, description: $description, price: $price, date: $date}) {
            _id
            title
            description
            price
            date
            creator {
                email
            }
        }
    }
`;

export const cancel = gql`
    mutation cancelEvent($eventId: ID!){
        cancelEvent(eventId: $eventId) {
            _id
            title
        }
    }
`;
