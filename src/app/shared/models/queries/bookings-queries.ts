import gql from 'graphql-tag';

export const getBookedEvents = gql`
    query bookings {
        bookings {
            _id
            event {
                title
            }
        }
    }
`;

export const getBooking = gql`
    query boking ($eventId: ID!){
        booking(eventId: $eventId) {
            _id
            user{
                email
            }
            event{
                title
            }

        }
    }
`;
