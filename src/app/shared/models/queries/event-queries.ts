import gql from 'graphql-tag';

export const getAllEvents = gql`
    query events {
        events {
            _id
            title
            canceled
            price
            date
            belongsToCurrentUser
            wasBookedByCurrentUser
            creator{
                email
            }
        }
    }
`;

export const getEvent = gql`
    query event($eventId: ID!){
        event(eventId: $eventId){
            _id
            title
            description
            canceled
            price
            date
            belongsToCurrentUser
            wasBookedByCurrentUser
            creator{
                email
            }
        }
    }
`;

export const getActiveEvents = gql`
    query activeEvents{
        activeEvents{
            _id
            title
            canceled
            price
            date
            belongsToCurrentUser
            wasBookedByCurrentUser
            creator{
                email
            }
        }
    }
`;

export const getUserEvents = gql`
    query userEvents {
        userEvents {
            _id
            title
            canceled
            price
            date
            creator{
                email
            }
        }
    }`;
