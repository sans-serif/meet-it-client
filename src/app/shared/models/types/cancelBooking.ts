/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: cancelBooking
// ====================================================

export interface cancelBooking_cancelBooking_event {
  __typename: "Event";
  _id: string;
}

export interface cancelBooking_cancelBooking {
  __typename: "Booking";
  _id: string;
  event: cancelBooking_cancelBooking_event;
}

export interface cancelBooking {
  cancelBooking: cancelBooking_cancelBooking;
}

export interface cancelBookingVariables {
  bookingId: string;
}
