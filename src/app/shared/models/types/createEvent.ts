/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: createEvent
// ====================================================

export interface createEvent_createEvent_creator {
  __typename: "User";
  email: string;
}

export interface createEvent_createEvent {
  __typename: "Event";
  _id: string;
  title: string;
  description: string;
  price: number;
  date: string;
  creator: createEvent_createEvent_creator;
}

export interface createEvent {
  createEvent: createEvent_createEvent | null;
}

export interface createEventVariables {
  title: string;
  description: string;
  price: number;
  date: string;
}
