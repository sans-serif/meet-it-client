/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: bookings
// ====================================================

export interface bookings_bookings_event {
  __typename: "Event";
  title: string;
}

export interface bookings_bookings {
  __typename: "Booking";
  _id: string;
  event: bookings_bookings_event;
}

export interface bookings {
  bookings: bookings_bookings[];
}
