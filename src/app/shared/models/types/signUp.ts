/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: signUp
// ====================================================

export interface signUp_signUp {
  __typename: "User";
  _id: string;
  email: string;
}

export interface signUp {
  signUp: signUp_signUp | null;
}

export interface signUpVariables {
  email: string;
  password: string;
}
