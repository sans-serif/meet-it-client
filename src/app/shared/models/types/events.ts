/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: events
// ====================================================

export interface events_events_creator {
  __typename: "User";
  email: string;
}

export interface events_events {
  __typename: "Event";
  _id: string;
  title: string;
  canceled: boolean;
  price: number;
  date: string;
  belongsToCurrentUser: boolean;
  wasBookedByCurrentUser: boolean;
  creator: events_events_creator;
}

export interface events {
  events: events_events[];
}
