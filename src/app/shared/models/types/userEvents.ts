/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: userEvents
// ====================================================

export interface userEvents_userEvents_creator {
  __typename: "User";
  email: string;
}

export interface userEvents_userEvents {
  __typename: "Event";
  _id: string;
  title: string;
  canceled: boolean;
  price: number;
  date: string;
  creator: userEvents_userEvents_creator;
}

export interface userEvents {
  userEvents: userEvents_userEvents[];
}
