/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: cancelEvent
// ====================================================

export interface cancelEvent_cancelEvent {
  __typename: "Event";
  _id: string;
  title: string;
}

export interface cancelEvent {
  cancelEvent: cancelEvent_cancelEvent;
}

export interface cancelEventVariables {
  eventId: string;
}
