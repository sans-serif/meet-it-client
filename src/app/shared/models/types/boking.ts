/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: boking
// ====================================================

export interface boking_booking_user {
  __typename: "User";
  email: string;
}

export interface boking_booking_event {
  __typename: "Event";
  title: string;
}

export interface boking_booking {
  __typename: "Booking";
  _id: string;
  user: boking_booking_user;
  event: boking_booking_event;
}

export interface boking {
  booking: boking_booking;
}

export interface bokingVariables {
  eventId: string;
}
