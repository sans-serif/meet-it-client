/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: activeEvents
// ====================================================

export interface activeEvents_activeEvents_creator {
  __typename: "User";
  email: string;
}

export interface activeEvents_activeEvents {
  __typename: "Event";
  _id: string;
  title: string;
  canceled: boolean;
  price: number;
  date: string;
  belongsToCurrentUser: boolean;
  wasBookedByCurrentUser: boolean;
  creator: activeEvents_activeEvents_creator;
}

export interface activeEvents {
  activeEvents: activeEvents_activeEvents[];
}
