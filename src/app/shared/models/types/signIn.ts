/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: signIn
// ====================================================

export interface signIn_signIn {
  __typename: "AuthData";
  userId: string;
  token: string;
  tokenExpiration: number;
}

export interface signIn {
  signIn: signIn_signIn;
}

export interface signInVariables {
  email: string;
  password: string;
}
