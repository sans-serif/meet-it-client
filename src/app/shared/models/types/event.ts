/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: event
// ====================================================

export interface event_event_creator {
  __typename: "User";
  email: string;
}

export interface event_event {
  __typename: "Event";
  _id: string;
  title: string;
  description: string;
  canceled: boolean;
  price: number;
  date: string;
  belongsToCurrentUser: boolean;
  wasBookedByCurrentUser: boolean;
  creator: event_event_creator;
}

export interface event {
  event: event_event;
}

export interface eventVariables {
  eventId: string;
}
