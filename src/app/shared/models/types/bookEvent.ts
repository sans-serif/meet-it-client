/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: bookEvent
// ====================================================

export interface bookEvent_bookEvent_event_creator {
  __typename: "User";
  email: string;
}

export interface bookEvent_bookEvent_event {
  __typename: "Event";
  _id: string;
  title: string;
  creator: bookEvent_bookEvent_event_creator;
}

export interface bookEvent_bookEvent_user {
  __typename: "User";
  email: string;
}

export interface bookEvent_bookEvent {
  __typename: "Booking";
  _id: string;
  event: bookEvent_bookEvent_event;
  user: bookEvent_bookEvent_user;
}

export interface bookEvent {
  bookEvent: bookEvent_bookEvent;
}

export interface bookEventVariables {
  eventId: string;
}
