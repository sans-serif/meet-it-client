import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
    @Input()
    user: any;

    @Output()
    navigate: EventEmitter<string> = new EventEmitter<string>();

    constructor() {
    }

    ngOnInit() {
    }

    navigateToLogin() {
        this.navigate.emit('login');
    }

    navigateToRegistration() {
        this.navigate.emit('sing-up');
    }

    logOut() {
        this.navigate.emit('logout');
    }

}
