import {Component, OnInit} from '@angular/core';
import {AuthHelperService} from '../../core/services/auth/auth-helper.service';
import {Router} from '@angular/router';
import {AuthRepositoryService} from '../../core/repositories/auth/auth-repository.service';

@Component({
    selector: 'app-public',
    templateUrl: './public.component.html',
    styleUrls: ['./public.component.scss']
})
export class PublicComponent implements OnInit {
    public userData: any;

    constructor(
        private router: Router,
        private authRepository: AuthRepositoryService) {
    }

    ngOnInit() {
        this.userData = AuthHelperService.getUserData();
    }

    navigationEmitter(event) {
        switch (event) {
            case 'login':
                this.router.navigate(['auth/sign-in']);
                break;
            case 'sing-up':
                this.router.navigate(['auth/sign-up']);
                break;
            case 'logout':
                AuthHelperService.logOut();
                this.authRepository.singOut();
                this.router.navigate(['auth/sign-in']);
                break;
        }
    }
}
