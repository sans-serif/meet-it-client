import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../shared/shared.module';
import { PublicRoutingModule } from './public-routing.module';
import { PublicComponent } from './public.component';
import { EventsListComponent } from './events-list/events-list.component';
import { EventDetailComponent } from './event-detail/event-detail.component';


@NgModule({
    declarations: [
        PublicComponent,
        EventsListComponent,
        EventDetailComponent
    ],
    imports: [
        CommonModule,
        PublicRoutingModule,
        SharedModule
    ]
})
export class PublicModule {
}
