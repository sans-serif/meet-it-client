import {Component, OnDestroy, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AuthHelperService} from '../../../core/services/auth/auth-helper.service';
import {EventsRepositoryService} from '../../../core/repositories/events/events-repository.service';
import {BookingsRepositoryService} from '../../../core/repositories/bookings/bookings-repository.service';
import {Subscription} from 'rxjs';

@Component({
    selector: 'app-events-list',
    templateUrl: './events-list.component.html',
    styleUrls: ['./events-list.component.scss']
})
export class EventsListComponent implements OnInit {
    public eventsList: [];
    public currentUser: any;

    constructor(
        private router: Router,
        private authHelper: AuthHelperService,
        private eventsRepository: EventsRepositoryService,
        private bookingsRepository: BookingsRepositoryService
    ) {
    }

    ngOnInit() {
        this.currentUser = AuthHelperService.getUserData();
        this.loadEvents();
    }

    loadEvents() {
        this.eventsRepository.loadActiveEvents()
            .subscribe(
                ({activeEvents}: any) => this.eventsList = activeEvents,
                (error: any) => console.log(error),
                () => console.log('onComplete')
            );
    }

    bookEvent(event) {
        if (!this.authHelper.isAuthenticated()) {
            this.authHelper.redirectUrl = this.router.url;
            this.router.navigate([`auth/sign-in`]);
        } else {
            this.bookingsRepository.bookEvent(event._id)
                .subscribe(
                    ({bookEvent}: any) => console.log(bookEvent),
                    (error: any) => console.log(error),
                    () => console.log('onCompleted')
                );
        }
    }

    cancelBooking(event) {
        this.bookingsRepository.loadBooking(event._id)
            .subscribe(({booking}) => {
                this.bookingsRepository.cancelBooking(booking._id)
                    .subscribe(({cancelBooking}: any) => console.log(cancelBooking));
            });
    }

    canBooking(event): boolean {
        return !this.currentUser || !event.belongsToCurrentUser && !event.wasBookedByCurrentUser;
    }

    canCancelBooking(event): boolean {
        return this.currentUser && event.wasBookedByCurrentUser;
    }
}
