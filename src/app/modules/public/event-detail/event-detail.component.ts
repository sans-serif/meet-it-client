import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthHelperService} from '../../../core/services/auth/auth-helper.service';
import {EventsRepositoryService} from '../../../core/repositories/events/events-repository.service';
import {BookingsRepositoryService} from '../../../core/repositories/bookings/bookings-repository.service';

@Component({
    selector: 'app-event-detail',
    templateUrl: './event-detail.component.html',
    styleUrls: ['./event-detail.component.scss']
})
export class EventDetailComponent implements OnInit {
    public event: any;
    public currentUser: any;
    private readonly currentEventId;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private authHelper: AuthHelperService,
        private eventsRepository: EventsRepositoryService,
        private bookingsRepository: BookingsRepositoryService
    ) {
        this.currentEventId = route.snapshot.paramMap.get('id');
    }

    ngOnInit() {
        this.currentUser = AuthHelperService.getUserData();
        this.loadEvent();
    }

    loadEvent() {
        this.eventsRepository.loadEvent(this.currentEventId)
            .subscribe(
                (data: any) => this.event = data.event,
                (error: any) => console.log(error),
                () => console.log('onCompleted')
            );
    }

    // TODO: add booked event to store
    bookEvent(event) {
        if (!this.authHelper.isAuthenticated()) {
            this.authHelper.redirectUrl = this.router.url;
            this.router.navigate([`auth/sign-in`]);
        } else {
            this.bookingsRepository.bookEvent(event._id)
                .subscribe(
                    (data: any) => console.log(data),
                    (error: any) => console.log(error),
                    () => console.log('onCompleted')
                );
        }
    }


    cancelBooking(event) {
        this.bookingsRepository.loadBooking(event._id)
            .subscribe(({booking}) => {
                this.bookingsRepository.cancelBooking(booking._id)
                    .subscribe(({cancelBooking}: any) => console.log(cancelBooking));
            });
    }

    canBooking(event): boolean {
        return !this.currentUser || !event.belongsToCurrentUser && !event.wasBookedByCurrentUser;
    }

    canCancelBooking(event): boolean {
        return this.currentUser && event.wasBookedByCurrentUser;
    }
}
