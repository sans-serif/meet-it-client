import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthRepositoryService} from '../../../core/repositories/auth/auth-repository.service';
import {AuthHelperService} from '../../../core/services/auth/auth-helper.service';
import {Router} from '@angular/router';
import {signIn, signInVariables} from '../../../shared/models/types/signIn';

@Component({
    selector: 'app-sign-in',
    templateUrl: './sign-in.component.html',
    styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit {
    public formSignIn: FormGroup;
    private formSubmitAttempt: boolean;

    constructor(
        private fb: FormBuilder,
        private router: Router,
        private authHelper: AuthHelperService,
        private authRepository: AuthRepositoryService) {
    }

    ngOnInit() {
        this.formSignIn = this.fb.group({
            email: [null, Validators.required],
            password: [null, Validators.required]
        });
    }

    isFieldInvalid(field: string) {
        return (!this.formSignIn.get(field).valid && this.formSignIn.get(field).touched) ||
            (this.formSignIn.get(field).untouched && this.formSubmitAttempt);
    }

    onSubmit() {
        this.formSubmitAttempt = true;

        if (!this.formSignIn.valid) {
            console.log('form invalid');
            return;
        }

        const userToLogin: signInVariables = this.formSignIn.value;

        this.authRepository.signIn(userToLogin)
            .subscribe(
                (result: signIn) => {
                    AuthHelperService.setAuthData(result.signIn);
                    AuthHelperService.setUserData(this.authHelper.getUserDataFromToken());
                    this.router.navigate([this.authHelper.redirectUrl || 'admin']);
                },
                (error) => {
                    if (error.length > 0) {
                        const unauthenticated = error.some(err => err.message === 'Password is incorrect!' ||
                            err.message === 'User does not exist!');

                        console.log(error);
                        if (unauthenticated) {
                            AuthHelperService.setUserData(null);
                        }
                    }
                }
            );

    }
}
