import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthRepositoryService} from '../../../core/repositories/auth/auth-repository.service';
import {Router} from '@angular/router';
import {signUp, signUpVariables} from '../../../shared/models/types/signUp';

@Component({
    selector: 'app-sign-up',
    templateUrl: './sign-up.component.html',
    styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {
    public formSignUp: FormGroup;
    private formSubmitAttempt: boolean;

    constructor(
        private fb: FormBuilder,
        private router: Router,
        private authRepository: AuthRepositoryService) {
    }

    ngOnInit() {
        this.formSignUp = this.fb.group({
            email: [null, Validators.required],
            password: [null, Validators.required]
        });
    }

    isFieldInvalid(field: string) {
        return (!this.formSignUp.get(field).valid && this.formSignUp.get(field).touched) ||
            (this.formSignUp.get(field).untouched && this.formSubmitAttempt);
    }

    onSubmit() {
        this.formSubmitAttempt = true;

        if (!this.formSignUp.valid) {
            console.log('form invalid');
            return;
        }

        const userToCreate: signUpVariables = this.formSignUp.value;

        this.authRepository.signUp(userToCreate)
            .subscribe(
                (result: signUp) => this.router.navigate(['/auth/sign-in']).then(() => alert(`Successful created user
                ${result.signUp.email}`)),
                err => console.log(err)
            );

    }
}
