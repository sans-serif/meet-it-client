import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthRoutingModule } from './auth-routing.module';
import { SharedModule } from '../../shared/shared.module';
import { AuthComponent } from './auth.component';
import { SignInComponent } from './sing-in/sign-in.component';
import { SignUpComponent } from './sing-up/sign-up.component';

@NgModule({
    declarations: [
        AuthComponent,
        SignInComponent,
        SignUpComponent
    ],
    imports: [
        CommonModule,
        AuthRoutingModule,
        SharedModule
    ]
})
export class AuthModule {
}
