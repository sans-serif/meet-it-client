import {Component, OnInit} from '@angular/core';
import {BookingsRepositoryService} from '../../../core/repositories/bookings/bookings-repository.service';

@Component({
    selector: 'app-bookings',
    templateUrl: './bookings.component.html',
    styleUrls: ['./bookings.component.scss']
})
export class BookingsComponent implements OnInit {
    public bookingsList: [];

    constructor(private bookingsRepository: BookingsRepositoryService) {
    }

    ngOnInit() {
        this.bookingsRepository.loadBookings()
            .subscribe(({bookings}: any) => this.bookingsList = bookings);
    }

    cancelBooking(booking) {
        this.bookingsRepository.cancelBooking(booking._id)
            .subscribe(({cancelBooking}: any) => console.log(cancelBooking));
    }

}
