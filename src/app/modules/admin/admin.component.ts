import {Component, OnInit} from '@angular/core';
import {AuthHelperService} from '../../core/services/auth/auth-helper.service';
import {Router} from '@angular/router';
import {AuthRepositoryService} from '../../core/repositories/auth/auth-repository.service';
import {UserData} from '../../shared/models/user-data';

@Component({
    selector: 'app-admin',
    templateUrl: './admin.component.html',
    styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
    public userData: UserData;

    constructor(
        private router: Router,
        private authRepository: AuthRepositoryService) {
    }

    ngOnInit() {
        this.userData = AuthHelperService.getUserData();
    }

    navigationEmitter(event) {
        switch (event) {
            case 'login':
                this.router.navigate(['auth/sign-in']);
                break;
            case 'sing-up':
                this.router.navigate(['auth/sign-up']);
                break;
            case 'logout':
                this.router.navigate(['auth/sign-in']).then(() => {
                    AuthHelperService.logOut();
                    this.authRepository.singOut();
                });
                break;
        }
    }

}
