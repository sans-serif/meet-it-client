import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
    selector: 'app-events-list',
    templateUrl: './events-list.component.html',
    styleUrls: ['./events-list.component.scss']
})
export class EventsListComponent implements OnInit {
    @Input()
    public formIsOpen;

    @Input()
    public userEventsList: [];

    @Output()
    private cancelEventEventEmitter: EventEmitter<any> = new EventEmitter<any>();

    @Output()
    private openFormEventEmitter: EventEmitter<string> = new EventEmitter<string>();

    constructor() {
    }

    ngOnInit() {
    }

    openForm(eventId) {
        this.openFormEventEmitter.emit(eventId);
    }

    cancelEvent(event) {
        this.cancelEventEventEmitter.emit(event);
    }
}
