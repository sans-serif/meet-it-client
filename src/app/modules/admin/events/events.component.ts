import {Component, OnInit} from '@angular/core';
import {EventsRepositoryService} from '../../../core/repositories/events/events-repository.service';

@Component({
    selector: 'app-events',
    templateUrl: './events.component.html',
    styleUrls: ['./events.component.scss']
})
export class EventsComponent implements OnInit {
    public userEventsList: [];
    public userEvent: any;
    public formIsOpen: boolean;

    constructor(private eventsRepository: EventsRepositoryService) {
    }

    ngOnInit() {
        this.eventsRepository.loadUserEvents()
            .subscribe(
                ({userEvents}: any) => this.userEventsList = userEvents,
                (error: any) => console.log(error),
                () => console.log('onComplete')
            );
    }

    openForm(eventId) {
        if (eventId) {
            this.eventsRepository.loadEvent(eventId)
                .subscribe(
                    ({event}: any) => {
                        this.userEvent = event;
                        this.formIsOpen = true;
                    },
                    (error: any) => console.log(error),
                    () => console.log('onComplete')
                );
        } else {
            this.userEvent = {};
            this.formIsOpen = true;
        }
    }

    closeForm() {
        this.formIsOpen = false;
    }

    createEvent(event) {
        this.eventsRepository.createEvent(event)
            .subscribe(
                ({createEvent}: any) => {
                    console.log(createEvent);
                },
                err => console.log(err)
            );

    }

    updateEvent(event) {
        this.eventsRepository.updateEvent(event)
            .subscribe(
                ({createEvent}: any) => {
                    console.log(createEvent);
                },
                err => console.log(err)
            );
    }

    cancelEvent(event) {
        this.eventsRepository.cancelEvent(event._id)
            .subscribe(({cancelEvent}: any) => console.log(cancelEvent));
    }
}
