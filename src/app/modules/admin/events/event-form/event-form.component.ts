import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
    selector: 'app-event-form',
    templateUrl: './event-form.component.html',
    styleUrls: ['./event-form.component.scss']
})
export class EventFormComponent implements OnInit, OnChanges {
    public formEvent: FormGroup;
    private formSubmitAttempt: boolean;

    @Input()
    public formIsOpen;

    @Input()
    public userEvent: any;

    @Output()
    private closeFormEventEmitter: EventEmitter<boolean> = new EventEmitter<boolean>();

    @Output()
    private createEventEventEmitter: EventEmitter<any> = new EventEmitter<any>();

    @Output()
    private updateEventEventEmitter: EventEmitter<any> = new EventEmitter<any>();

    constructor(private fb: FormBuilder) {
    }

    ngOnInit() {
        this.formEvent = this.fb.group({
            title: [null, Validators.required],
            description: [null, Validators.required],
            price: [null, Validators.required],
            date: [null, Validators.required]
        });
    }

    ngOnChanges() {
        if (this.userEvent) {
            this.formEvent = this.fb.group({
                title: [this.userEvent.title, Validators.required],
                description: [this.userEvent.description, Validators.required],
                price: [this.userEvent.price, Validators.required],
                date: [this.userEvent.date, Validators.required]
            });
        }
    }

    closeForm() {
        this.closeFormEventEmitter.emit(true);
    }

    isFieldInvalid(field: string) {
        return (!this.formEvent.get(field).valid && this.formEvent.get(field).touched) ||
            (this.formEvent.get(field).untouched && this.formSubmitAttempt);
    }

    onSubmit() {
        this.formSubmitAttempt = true;

        if (!this.formEvent.valid) {
            console.log('form invalid');
        } else {
            if (this.formEvent.value.id) {
                this.updateEventEventEmitter.emit(this.formEvent.value);
            } else {
                this.createEventEventEmitter.emit(this.formEvent.value);
            }
        }
    }
}
