import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageNotFoundComponent } from '../../shared/page-not-found/page-not-found.component';
import { EventDetailComponent } from './events/event-detail/event-detail.component';
import {EventsComponent} from './events/events.component';
import {BookingsComponent} from './bookings/bookings.component';

const routes: Routes = [
    { path: '', redirectTo: 'my-events', pathMatch: 'full' },
    { path: 'my-events', component: EventsComponent },
    { path: 'my-events/:id', component: EventDetailComponent },
    { path: 'my-bookings', component: BookingsComponent },
    { path: '**', component: PageNotFoundComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AdminRoutingModule {
}
